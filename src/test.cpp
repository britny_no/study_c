#include <string>
#include <iostream>
#include <bitset>

int main ()
{
    using namespace std;

    // int + int, float * float는 타입 단위 연산자
    // 비트 단위 연산자는 아래와 같다
    // << left shift(비트를 전부 왼쪽으로 한칸씩 이동)
    // >> right shift(비트를 전부 오른쪽으로 한칸씩 이동)
    // ~ not(0 -> 1, 1 -> 0으로 변환), 
    // & and(같은 자리에 위치한 비트가 모두 1일경우에만 1 도출),
    // | or(같은 자리에 위치한 비트가 하나만 1이어도 1 도출), 
    // ^ xor(같은 자리에 위치한 비트가 하나만 1이어도 1인데, 만약 둘다 1이면 0)

    unsigned int a = 0b1100;
    unsigned int b = 0b0110;

    cout << a << endl;
    cout << b << endl;

    cout << bitset<4>(a & b) << endl;
    cout << bitset<4>(a | b) << endl;
    cout << bitset<4>(a ^ b) << endl;

    


    return 0;
}